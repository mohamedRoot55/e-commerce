//import 'package:after_layout/after_layout.dart';
//import 'package:auto_size_text/auto_size_text.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter/rendering.dart';
//
//import '../../common/styles.dart';
//import '../../models/category.dart';
//import '../../models/product.dart';
//
//class CardCategories extends StatefulWidget {
//  final List<Category> categories;
//
//  CardCategories(this.categories);
//
//  @override
//  _StateCardCategories createState() => _StateCardCategories();
//}
//
//class _StateCardCategories extends State<CardCategories> with AfterLayoutMixin {
//  ScrollController controller = ScrollController();
//  double page;
//
//  @override
//  void initState() {
//    page = 0.0;
//    super.initState();
//  }
//
//  @override
//  void afterFirstLayout(BuildContext context) {
//    final screenSize = MediaQuery.of(context).size;
//    controller.addListener(() {
//      setState(() {
//        page = _getPage(controller.position, screenSize.width * 0.30 + 10);
//      });
//    });
//  }
//
//  bool hasChildren(id) {
//    return widget.categories.where((o) => o.parent == id).toList().length > 0;
//  }
//
//  double _getPage(ScrollPosition position, double width) {
//    return position.pixels / width;
//  }
//
//  List<Category> getSubCategories(id) {
//    return widget.categories.where((o) => o.parent == id).toList();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    final screenSize = MediaQuery.of(context).size;
//    final _categories =
//        widget.categories.where((item) => item.parent == 0).toList();
//
//    return Container(
//      height: screenSize.height,
//      child: Stack(
//        overflow: Overflow.visible,
//        fit: StackFit.loose,
//        children: <Widget>[
//          ClipPath(
//            clipper: ClippingClass(),
//            child: Container(
//              width: double.infinity,
//              height: MediaQuery.of(context).size.height * 4 / 7,
//              decoration: BoxDecoration(
//                gradient: LinearGradient(
//                  begin: Alignment.topCenter,
//                  end: Alignment.bottomCenter,
//                  colors: [Color(0xff40dedf), Color(0xff0fb2ea)],
//                ),
//              ),
//            ),
//          ),
//          SingleChildScrollView(
//              controller: controller,
//              scrollDirection: Axis.vertical,
//              child: Column(
//                children: <Widget>[
//
//                  SingleChildScrollView(
//
//                    child: Container(
//                   //   height: screenSize.height * .6,
//                      child: Padding(
//                        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
//                        child: GridView.builder(
//                         shrinkWrap: true,
////                          reverse: true,
////                          primary: true,
//                          gridDelegate:
//                              SliverGridDelegateWithFixedCrossAxisCount(
//
//                                  crossAxisCount: 2,
//                                  childAspectRatio: 3 / 2,
//                                  mainAxisSpacing: 10,
//                                  crossAxisSpacing: 10),
//                          itemBuilder: (ctx, index) => CategoryCardItem(
//                            _categories[index],
//                            offset: page - index,
//                            hasChildren: hasChildren(_categories[index].id),
//                          ),
//                          itemCount: _categories.length,
//                        ),
//                      ),
//                    ),
//                  ),
//                  SizedBox(height: 20),
//                  Padding(
//                    padding: const EdgeInsets.only(right: 10, left: 10),
//                    child: AutoSizeText(
//                      "Get your laundry washed, pressed, folded and delivered straight to your door in just 3 simple steps.",
//                      style: TextStyle(
//                        color: Colors.black,
//                        fontSize: 18,
//                      ),
//                    ),
//                  ),
//                  SizedBox(
//                    height: screenSize.height * .02,
//                  ),
//                  Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceAround,
//                    children: <Widget>[
//                      Column(
//                        children: <Widget>[
//                          Container(
//                              height: screenSize.height * .1,
//                              width: screenSize.width * .2,
//                              padding: const EdgeInsets.all(15),
//                              decoration: BoxDecoration(
//                                  borderRadius: BorderRadius.circular(20),
//                                  boxShadow: [
//                                    BoxShadow(
//                                        color: Color(0xff40dedf),
//                                        blurRadius: 0.5,
//                                        offset: Offset(0.0, 0.0)),
//                                  ]),
//                              child: ClipRRect(
//                                  borderRadius: BorderRadius.circular(30),
//                                  child:
//                                      Image.asset("assets/images/home1.png"))),
//                          SizedBox(
//                            height: 10,
//                          ),
//                          Container(
//                            //   width: ScreenWidth * .2,
//                            child: FittedBox(
//                              fit: BoxFit.contain,
//                              child: AutoSizeText(
//                                "Choose laundry\n service/s",
//                                style: TextStyle(
//                                    color: Colors.black,
//                                    fontWeight: FontWeight.bold,
//                                    fontSize: 9,
//                                    fontStyle: FontStyle.italic),
//                              ),
//                            ),
//                          )
//                        ],
//                      ),
//                      Column(
//                        children: <Widget>[
//                          Container(
//                              padding: const EdgeInsets.all(15),
//                              height: screenSize.height * .1,
//                              width: screenSize.width * .2,
//                              decoration: BoxDecoration(
//                                  borderRadius: BorderRadius.circular(20),
//                                  boxShadow: [
//                                    BoxShadow(
//                                        color: Color(0xff40dedf),
//                                        blurRadius: 0.5,
//                                        offset: Offset(0.0, 0.0)),
//                                  ]),
//                              child: ClipRRect(
//                                  borderRadius: BorderRadius.circular(30),
//                                  child: Image.asset(
//                                      "assets/images/rocket1.png"))),
//                          SizedBox(
//                            height: 10,
//                          ),
//                          Container(
//                            //  width: screenSize.width * .3,
//                            child: FittedBox(
//                              fit: BoxFit.contain,
//                              child: AutoSizeText(
//                                "Add your staff\n to the bag",
//                                style: TextStyle(
//                                    fontSize: 9,
//                                    color: Colors.black,
//                                    fontWeight: FontWeight.bold,
//                                    fontStyle: FontStyle.italic),
//                              ),
//                            ),
//                          )
//                        ],
//                      ),
//                      Column(
//                        children: <Widget>[
//                          Container(
//                              height: screenSize.height * .1,
//                              width: screenSize.width * .2,
//                              padding: const EdgeInsets.all(15),
//                              decoration: BoxDecoration(
//                                  borderRadius: BorderRadius.circular(20),
//                                  boxShadow: [
//                                    BoxShadow(
//                                        color: Color(0xff40dedf),
//                                        blurRadius: 0.5,
//                                        offset: Offset(0.0, 0.0)),
//                                  ]),
//                              child: ClipRRect(
//                                borderRadius: BorderRadius.circular(30),
//                                child: Image.asset(
//                                  "assets/images/location.png",
//                                  fit: BoxFit.cover,
//                                ),
//                              )),
//                          SizedBox(
//                            height: 10,
//                          ),
//                          FittedBox(
//                            fit: BoxFit.contain,
//                            child: AutoSizeText(
//                              "Review your bag\n and Checkout",
//                              style: TextStyle(
//                                  color: Colors.black,
//                                  fontWeight: FontWeight.bold,
//                                  fontSize: 9,
//                                  fontStyle: FontStyle.italic),
//                            ),
//                          ),
//                        ],
//                      )
//                    ],
//                  ),
//                ],
//              ))
//        ],
//      ),
//    );
//  }
//}
//
//class CategoryCardItem extends StatelessWidget {
//  final Category category;
//  final bool hasChildren;
//  final offset;
//
//  CategoryCardItem(this.category, {this.hasChildren = false, this.offset});
//
//  @override
//  Widget build(BuildContext context) {
//    final screenSize = MediaQuery.of(context).size;
//
//    return GestureDetector(
//        onTap: hasChildren
//            ? null
//            : () {
//                Product.showList(
//                    context: context,
//                    cateId: category.id,
//                    cateName: category.name);
//              },
//        child: Container(
//          height: screenSize.width * .5,
//          // width: screenSize.width * .35,
//          padding: EdgeInsets.only(left: 10, right: 10),
//         margin: EdgeInsets.only(bottom: 10),
//          child: Card(
//            color: Colors.white,
//            shape:
//                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
//            elevation: 7,
//            child: Padding(
//
//              padding: EdgeInsets.all(10),
//              child: Column(
//                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                crossAxisAlignment: CrossAxisAlignment.center,
//                children: <Widget>[
//                  // todo  make the image size dynamic to make it responsive
//                  Container(
//                    height: screenSize.height * .08,
//                    width: screenSize.width * .4,
//                    // todo make image take all space aviable
//                    child: Padding(
//                      padding: const EdgeInsets.all(2.0),
//                      child: Image.network(
//                        category.image,
//                        fit: BoxFit.contain,
//                      ),
//                    ),
//                  ),
//                  Align(
//                    alignment: Alignment.bottomLeft,
//                    child: Column(
//                      crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        Container(
//                          // width : MediaQuery.of(context).size.width * .3 ,
//
//                          child: FittedBox(
//                            fit: BoxFit.cover,
//                            child: AutoSizeText(
//                              category.name,
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  )
//                ],
//              ),
//            ),
//          ),
//        ));
//  }
//}
//
//class SubItem extends StatelessWidget {
//  final Category category;
//  final bool isLast;
//
//  SubItem(this.category, {this.isLast = false});
//
//  @override
//  Widget build(BuildContext context) {
//    final screenSize = MediaQuery.of(context).size;
//
//    return Container(
//      width: screenSize.width,
//      child: FittedBox(
//        fit: BoxFit.cover,
//        child: Container(
//          width:
//              screenSize.width / (2 / (screenSize.height / screenSize.width)),
//          padding: EdgeInsets.symmetric(vertical: 5),
//          margin: EdgeInsets.symmetric(horizontal: 10),
//          decoration: BoxDecoration(
//              border: Border(bottom: BorderSide(color: kGrey200))),
//          child: Row(
//            children: <Widget>[
//              SizedBox(
//                width: isLast ? 50 : 20,
//              ),
//              Expanded(child: Text(category.name)),
//              InkWell(
//                onTap: () {
//                  Product.showList(
//                      context: context,
//                      cateId: category.id,
//                      cateName: category.name);
//                },
//                child: Text(
//                  "${category.totalProduct} items",
//                  style: TextStyle(
//                      fontSize: 14, color: Theme.of(context).primaryColor),
//                ),
//              ),
//              IconButton(
//                  icon: Icon(Icons.keyboard_arrow_right),
//                  onPressed: () {
//                    Product.showList(
//                        context: context,
//                        cateId: category.id,
//                        cateName: category.name);
//                  })
//            ],
//          ),
//        ),
//      ),
//    );
//  }
//}
//
//class ClippingClass extends CustomClipper<Path> {
//  @override
//  Path getClip(Size size) {
//    var path = Path();
//    path.lineTo(0.0, size.height);
//    var controlPoint = Offset(size.width - (size.width / 2), size.height - 120);
//    var endPoint = Offset(size.width, size.height);
//    path.quadraticBezierTo(
//        controlPoint.dx, controlPoint.dy, endPoint.dx, endPoint.dy);
//    path.lineTo(size.width, 0.0);
//    path.close();
//    return path;
//  }
//
//  @override
//  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
//}
import 'package:after_layout/after_layout.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fstore/generated/i18n.dart';
import 'package:fstore/providers/ArabicProvider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../common/styles.dart';
import '../../models/category.dart';
import '../../models/product.dart';

class CardCategories extends StatefulWidget {
  final List<Category> categories;

  CardCategories(this.categories);

  @override
  _StateCardCategories createState() => _StateCardCategories();
}

class _StateCardCategories extends State<CardCategories> with AfterLayoutMixin {
  ScrollController controller = ScrollController();
  double page;
  bool isInit = true;

  List<Category> mArabic_categories = [];

  List<Category> mEnglish_categories = [];
  SharedPreferences prefs;

  List<Category> _categories = [];

  @override
  void didChangeDependencies() async {
    prefs = await SharedPreferences.getInstance();
    if (isInit) {
      _categories =
          widget.categories.where((item) => item.parent == 0).toList();
      for (int i = 0; i < _categories.length; i++) {
        print(_categories[i].id.toString());
        if (_categories[i].id >= 38 && _categories[i].id <= 43) {
          print(_categories[i].id.toString());
          print('english ids ' + _categories[i].id.toString());
          setState(() {
            mEnglish_categories.add(_categories[i]);
          });
        }
        if (_categories[i].id >= 44 && _categories[i].id <= 49) {
          print(_categories[i].id.toString());
          print('arabic ids ' + _categories[i].id.toString());
          setState(() {
            mArabic_categories.add(_categories[i]);
          });
        }
      }

      isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  void initState() {
    page = 0.0;
//    Future.delayed(Duration(seconds: 0)).then((value) async {
//      prefs = await SharedPreferences.getInstance();
//    });

    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    controller.addListener(() {
      setState(() {
        page = _getPage(controller.position, screenSize.width * 0.30 + 10);
      });
    });
  }

  bool hasChildren(id) {
    return widget.categories.where((o) => o.parent == id).toList().length > 0;
  }

  double _getPage(ScrollPosition position, double width) {
    return position.pixels / width;
  }

  List<Category> getSubCategories(id) {
    return widget.categories.where((o) => o.parent == id).toList();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Container(

      height: screenSize.height,
      child: Stack(
        //  overflow: Overflow.visible,
        fit: StackFit.loose,
        children: <Widget>[
          ClipPath(
            clipper: ClippingClass(),
            child: Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 4 / 7,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Color(0xff40dedf), Color(0xff0fb2ea)],
                ),
              ),
            ),
          ),
          SingleChildScrollView(
              controller: controller,
              scrollDirection: Axis.vertical,
              child: Column(
                children: <Widget>[
                  !prefs.containsKey("FIRST_NAME") ||
                          prefs.getString("FIRST_NAME") == null
                      ? Padding(
                          padding: const EdgeInsets.only(
                              top: 28, left: 28, right: 28),
                          child: Align(
                            child: AutoSizeText(
                              S.of(context).Hi,
                              textDirection: TextDirection.rtl,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                            alignment: !prefs.containsKey("is_arabic") ||
                                    !prefs.getBool("is_arabic")
                                ? Alignment.topLeft
                                : Alignment.topRight,
                          ),
                        )
                      : Padding(
                          padding: const EdgeInsets.only(
                              top: 28, left: 28, right: 28),
                          child: Align(
                            child: AutoSizeText(
                              "${S.of(context).Hi} ${prefs.get("FIRST_NAME")} ",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                            alignment: !prefs.containsKey("is_arabic") ||
                                    !prefs.getBool("is_arabic")
                                ? Alignment.topLeft
                                : Alignment.topRight,
                          ),
                        ),
                  SingleChildScrollView(
                    child: Container(
                      //   height: screenSize.height * .6,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 11, vertical: 16),
                        child: GridView.builder(
                          shrinkWrap: true,
//                          reverse: true,
//                          primary: true,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  childAspectRatio: 2 / 1.5,
                                  mainAxisSpacing: 0,
                                  crossAxisSpacing: 0),
                          itemBuilder: (ctx, index) {
                            if (prefs.containsKey("is_arabic") == false &&
                                prefs.getBool("is_arabic") == null) {
                              if (Provider.of<SwitchCategoryProvider>(context)
                                  .getSwisherValue) {
                                return CategoryCardItem(
                                  mArabic_categories[index],
                                  offset: page - index,
                                  hasChildren:
                                      hasChildren(mArabic_categories[index].id),
                                );
                              } else {
                                return CategoryCardItem(
                                  mEnglish_categories[index],
                                  offset: page - index,
                                  hasChildren: hasChildren(
                                      mEnglish_categories[index].id),
                                );
                              }
                            } else {
                              if (prefs.getBool("is_arabic")) {
                                return CategoryCardItem(
                                  mArabic_categories[index],
                                  offset: page - index,
                                  hasChildren:
                                      hasChildren(mArabic_categories[index].id),
                                );
                              } else {
                                return CategoryCardItem(
                                  mEnglish_categories[index],
                                  offset: page - index,
                                  hasChildren: hasChildren(
                                      mEnglish_categories[index].id),
                                );
                              }
                            }
                          },
                          itemCount: mArabic_categories.length,
                        ),
                      ),
                    ),
                  ),
                  //  SizedBox(height: 5),
                  Padding(
                    padding: const EdgeInsets.only(
                      right: 10,
                      left: 10,
                    ),
                    child: AutoSizeText(
                      S.of(context).homeDescription,
                      style: TextStyle(
                      //  color: Colors.black,
                        fontSize: 18,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenSize.height * .02,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                              height: screenSize.height * .1,
                              width: screenSize.width * .2,
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color(0xff40dedf),
                                        blurRadius: 0.5,
                                        offset: Offset(0.0, 0.0)),
                                  ]),
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(30),
                                  child:
                                      Image.asset("assets/images/home1.png"))),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            //   width: ScreenWidth * .2,
                            child: FittedBox(
                              fit: BoxFit.contain,
                              child: AutoSizeText(
                                S.of(context).chooseYourService,
                                style: TextStyle(
                                 //   color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 7,
                                    fontStyle: FontStyle.italic),
                              ),
                            ),
                          )
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                              padding: const EdgeInsets.all(8),
                              height: screenSize.height * .1,
                              width: screenSize.width * .2,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color(0xff40dedf),
                                        blurRadius: 0.5,
                                        offset: Offset(0.0, 0.0)),
                                  ]),
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(30),
                                  child: Image.asset(
                                      "assets/images/rocket1.png"))),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            //  width: screenSize.width * .3,
                            child: FittedBox(
                              fit: BoxFit.contain,
                              child: AutoSizeText(
                                S.of(context).addStuff,
                                style: TextStyle(
                                    fontSize: 7,
                                   // color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.italic),
                              ),
                            ),
                          )
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                              height: screenSize.height * .1,
                              width: screenSize.width * .2,
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color(0xff40dedf),
                                        blurRadius: 0.5,
                                        offset: Offset(0.0, 0.0)),
                                  ]),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(30),
                                child: Image.asset(
                                  "assets/images/location.png",
                                  fit: BoxFit.cover,
                                ),
                              )),
                          SizedBox(
                            height: 5,
                          ),
                          FittedBox(
                            fit: BoxFit.contain,
                            child: AutoSizeText(
                              S.of(context).reviewBag,
                              style: TextStyle(
                                //  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 7,
                                  fontStyle: FontStyle.italic),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ],
              )),

//          Switch(
//            value: isArabic,
//            onChanged: (value) {
//              if (value) {
//                setState(() {
//                  isArabic = true;
//                });
//              } else {
//                setState(() {
//                  isArabic = false;
//                });
//              }
//            },
//          ),
        ],
      ),
    );
  }
}

class CategoryCardItem extends StatelessWidget {
  final Category category;
  final bool hasChildren;
  final offset;

  CategoryCardItem(this.category, {this.hasChildren = false, this.offset});

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return GestureDetector(
        onTap: hasChildren
            ? null
            : () {
                Product.showList(
                    context: context,
                    cateId: category.id,
                    cateName: category.name);
              },
        child: Container(
          height: screenSize.width * .6,
          // width: screenSize.width * .35,
          padding: EdgeInsets.only(left: 15, right: 15),
          margin: EdgeInsets.only(bottom: 15),
          child: Card(
            color: Theme.of(context).backgroundColor,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            elevation: 10,
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  // todo  make the image size dynamic to make it responsive
                  Container(
                    height: screenSize.height * .08,
                    width: screenSize.width * .4,
                    // todo make image take all space aviable
                    child: Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: Image.network(
                        category.image,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          // width : MediaQuery.of(context).size.width * .3 ,

                          child: Center(
                            child: FittedBox(
                              fit: BoxFit.cover,
                              child: Center(
                                child: AutoSizeText(
                                  category.name,
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }
}

class SubItem extends StatelessWidget {
  final Category category;
  final bool isLast;

  SubItem(this.category, {this.isLast = false});

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Container(
      width: screenSize.width,
      child: FittedBox(
        fit: BoxFit.cover,
        child: Container(
          width:
              screenSize.width / (2 / (screenSize.height / screenSize.width)),
          padding: EdgeInsets.symmetric(vertical: 5),
          margin: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: kGrey200))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                width: isLast ? 50 : 20,
              ),
              Expanded(
                  child: Center(
                      child: Text(
                category.name,
                textAlign: TextAlign.center,
              ))),
              InkWell(
                onTap: () {
                  Product.showList(
                      context: context,
                      cateId: category.id,
                      cateName: category.name);
                },
                child: Text(
                  "${category.totalProduct} items",
                  style: TextStyle(
                      fontSize: 14, color: Theme.of(context).primaryColor),
                ),
              ),
              IconButton(
                  icon: Icon(Icons.keyboard_arrow_right),
                  onPressed: () {
                    Product.showList(
                        context: context,
                        cateId: category.id,
                        cateName: category.name);
                  })
            ],
          ),
        ),
      ),
    );
  }
}

class ClippingClass extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0.0, size.height);
    var controlPoint = Offset(size.width - (size.width / 2), size.height - 120);
    var endPoint = Offset(size.width, size.height);
    path.quadraticBezierTo(
        controlPoint.dx, controlPoint.dy, endPoint.dx, endPoint.dy);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
