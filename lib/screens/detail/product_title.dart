import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/tools.dart';
import '../../models/product.dart';
import '../../models/app.dart';
import '../../widgets/start_rating.dart';
import '../../generated/i18n.dart';

class ProductTitle extends StatelessWidget {
  final Product product;

  ProductTitle(this.product);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    ProductVariation productVariation;
    productVariation = Provider.of<ProductModel>(context).productVariation;
    final currency = Provider.of<AppModel>(context).currency;

    final regularPrice = productVariation != null
        ? productVariation.regularPrice
        : product.regularPrice;
    final onSale =
        productVariation != null ? productVariation.onSale : product.onSale;
    final price =
        productVariation != null ? productVariation.price : product.price;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 10),
        Container(
          width: MediaQuery.of(context).size.width,
          child: Text(
            product.name,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
          ),
        ),
        SizedBox(height: 10),
        Row(
          children: <Widget>[
            Text(Tools.getCurrecyFormatted(price, currency: currency),
                style: Theme.of(context)
                    .textTheme
                    .headline
                    .copyWith(fontSize: 17, color: theme.accentColor)),
            if (onSale)
              Row(
                children: <Widget>[
                  SizedBox(width: 5),
                  Text(
                      Tools.getCurrecyFormatted(regularPrice,
                          currency: currency),
                      style: Theme.of(context).textTheme.headline.copyWith(
                          fontSize: 16,
                          color: Theme.of(context).accentColor,
                          decoration: TextDecoration.lineThrough)),
                  SizedBox(width: 5),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 5, vertical: 2),
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(5)
                    ),
                    child: Text(
                        S.of(context).sale('${int.parse(price) > 0 ? (((int.parse(regularPrice) - int.parse(price))  / int.parse(regularPrice)) * 100).toInt() : 100}'),
                      style: TextStyle(color: Colors.white, fontWeight: FontWeight.w700),
                    ),
                  )
                ],
              )
          ],
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10.0),
          child: Row(
            children: <Widget>[
              SmoothStarRating(
                  allowHalfRating: true,
                  starCount: 5,
                  rating: product.averageRating,
                  size: 17.0,
                  color: theme.primaryColor,
                  borderColor: theme.primaryColor,
                  spacing: 0.0),
              Text(
                " (${product.ratingCount})",
                style: Theme.of(context).textTheme.headline.copyWith(
                      fontSize: 12,
                      color: Theme.of(context).accentColor.withOpacity(0.8),
                    ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
