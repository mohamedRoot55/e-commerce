import 'package:flutter/material.dart';
import 'package:fstore/models/address.dart';
import 'package:fstore/services/locationServices.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:location/location.dart';

import '../common/config.dart' as config;
import '../common/styles.dart';

class OnBoardScreen extends StatefulWidget {
  @override
  _OnBoardScreenState createState() => _OnBoardScreenState();
}

class _OnBoardScreenState extends State<OnBoardScreen> {
  SharedPreferences preferences;
  Address address ;

  List<Slide> slides = [];
  final isRequiredLogin = config.kAdvanceConfig['IsRequiredLogin'];

  Future<void> initPref() async {
    return preferences = await SharedPreferences.getInstance();
  }

  Future<void> getLocation() async {
    return await LocationService().getLocation().then((location){
      preferences.setString("LOCATION", "https://maps.google.com/?q=${location.latitude},${location.longitude}" ) ;
      address.street = preferences.getString("LOCATION");
    });
  }


  @override
  void didChangeDependencies() async {
   await getLocation() ;
    super.didChangeDependencies() ;
  }

  @override
  void initState() {
    initPref();
    Widget loginWidget = Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Image.asset(
          "assets/images/location.png",
          height: 200,
          fit: BoxFit.contain,
        ),
      ],
    );

    for (int i = 0; i < config.onBoardingData.length; i++) {
      Slide slide = Slide(
        //  title: config.onBoardingData[i]['title'],
        description: config.onBoardingData[i]['desc'],

        maxLineTextDescription: 2,

        backgroundColor: Colors.white,
        marginDescription: EdgeInsets.fromLTRB(20.0, 50.0, 20.0, 0),
        styleDescription: TextStyle(
          fontSize: 28.0,
          fontWeight: FontWeight.bold,
          color: kGrey600,
        ),
        foregroundImageFit: BoxFit.fitWidth,
      );

      if (i == 2) {
        slide.centerWidget = loginWidget;
      } else {
        slide.pathImage = config.onBoardingData[i]['image'];
      }

      slides.add(slide);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return IntroSlider(
      slides: slides,
      styleNameSkipBtn: TextStyle(color: kGrey900),
      styleNameDoneBtn: TextStyle(color: kGrey900),
      isShowSkipBtn: false,
      nameNextBtn: 'Next',
      nameDoneBtn: 'Done',
      onDonePress: () async {
        Navigator.pushNamed(context, '/phoneAuth');
      },
    );
  }
}
