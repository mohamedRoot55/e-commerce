import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fstore/generated/i18n.dart';
import 'package:provider/provider.dart';

import '../../common/tools.dart';
import '../../models/cart.dart';
import '../../models/product.dart';
import '../../models/recent_product.dart';
import '../../models/app.dart';
import '../../screens/detail/index.dart';
import '../../widgets/heart_button.dart';
import '../start_rating.dart';
import '../../widgets/padge.dart';

class ProductCard extends StatelessWidget {
  void increaseCounter() {
    counter++;
  }

  int counter;
  final Product item;
  final width;
  final marginRight;
  final kSize size;
  final bool isHero;
  final bool showCart;
  final bool showHeart;
  final height;
  final bool hideDetail;
  final offset;
  final tablet;

  ProductCard(
      {this.counter = 0,
      this.item,
      this.width,
      this.size = kSize.medium,
      this.isHero = false,
      this.showHeart = false,
      this.showCart = false,
      this.height,
      this.offset,
      this.hideDetail = false,
      this.tablet,
      this.marginRight = 6.0});

  Widget getImageFeature(onTapProduct) {
    return GestureDetector(
      onTap: () {},
      child: isHero
          ? Hero(
              tag: 'product-${item.id}',
              child: Tools.image(
                url: item.imageFeature,
                width: width,
                size: kSize.medium,
                isResize: true,
                height: height ?? width * 0.8,
                fit: BoxFit.cover,
              ),
            )
          : Tools.image(
              url: item.imageFeature,
              width: width,
              size: kSize.medium,
              isResize: true,
              height: height ?? width * 0.8,
              fit: BoxFit.cover,
              offset: offset ?? 0.0,
            ),
    );
  }

  onTapProduct(context) {
    if (item.imageFeature == '') return;
    Provider.of<RecentModel>(context).addRecentProduct(item);
    print('item id: ${item.id}');
    Navigator.push(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) => Detail(product: item),
          fullscreenDialog: true,
        ));
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }

    // TODO according to DartDoc num.parse() includes both (double.parse and int.parse)
    return double.parse(s, (e) => null) != null ||
        int.parse(s, onError: (e) => null) != null;
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final addProductToCart = Provider.of<CartModel>(context).addProductToCart;
    final ProductId = Provider.of<CartModel>(context).productId;
    final currency = S.of(context).currency;
    final isTablet = tablet ?? Tools.isTablet(MediaQuery.of(context));
    double titleFontSize = isTablet ? 24.0 : 16.0;

    double iconSize = isTablet ? 30.0 : 30.0;
    double starSize = isTablet ? 20.0 : 10.0;
    final gauss = offset != null
        ? math.exp(-(math.pow((offset.abs() - 0.5), 2) / 0.08))
        : 0.0;

    if (hideDetail)
      return getImageFeature(
        () => onTapProduct(context),
      );

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Stack(
        children: <Widget>[
          Container(
            width: width,
            margin: EdgeInsets.all((5)),
            padding: EdgeInsets.all(8.0),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    color: Colors.black26.withOpacity(.7),
                    spreadRadius: .6,
                    blurRadius: .6,
                    offset: Offset(0.9, 0.6))
              ],
              color: Theme.of(context).cardColor,
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(2.0),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(0.0),
                        child: Transform.translate(
                          offset: Offset(18 * gauss, 0.0),
                          child: getImageFeature(
                            () => onTapProduct(context),
                          ),
                        ),
                      ),
                      if ((item.onSale ?? false) &&
                          item.regularPrice.isNotEmpty)
                        Align(
                            alignment: Alignment.topRight,
                            child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 6, vertical: 3),
                                decoration: BoxDecoration(
                                    color: Colors.redAccent,
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(8))),
                                child: Text(
                                  '${(100 - double.parse(item.price) / double.parse(item.regularPrice.toString()) * 100).toInt()} %',
                                  style: TextStyle(
                                      fontSize: 11, color: Colors.white),
                                )))
                    ],
                  ),
                ),
                Text(item.name ?? '',
                    style: TextStyle(
                      fontSize: titleFontSize,
                      fontWeight: FontWeight.w600,
                    ),
                    maxLines: 1),
                SizedBox(height: 4),
                Wrap(
                  children: <Widget>[
                    if (item.onSale ?? false)
                      Text(
                        Tools.getPriceProduct(item, currency, onSale: true),
                        style: Theme.of(context).textTheme.headline.copyWith(
                              fontSize: 12,
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.6),
                              decoration: TextDecoration.lineThrough,
                            ),
                      ),
                    if (item.onSale ?? false) SizedBox(width: 5),
                    Text(
                      Tools.getPriceProduct(item, currency, onSale: false),
                      style: Theme.of(context).textTheme.headline.copyWith(
                            fontSize: 12,
                            color: theme.accentColor,
                          ),
                    ),
                  ],
                ),
                SizedBox(height: 4),
                FittedBox(
                  fit: BoxFit.contain,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
//                  SmoothStarRating(
//                      allowHalfRating: true,
//                      starCount: 5,
//                      rating: item.averageRating ?? 0.0,
//                      size: starSize,
//                      color: theme.primaryColor,
//                      borderColor: theme.primaryColor,
//                      spacing: 0.0),
                      if (showCart && !item.isEmptyProduct())
                        FlatButton(
                            onPressed: () {
                              addProductToCart(
                                product: item,
                              );

                              increaseCounter();
                              Fluttertoast.showToast(
                                  msg: "A new item added to your Bag",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.TOP,
                                  timeInSecForIos: 1,
                                  backgroundColor: Colors.blueAccent,
                                  textColor: Colors.white,
                                  fontSize: 14.0);
                              print(
                                  "this is a product id from product card >> ${CartModel().productsInCart[item.id.toString()]}");
                              print(
                                  "this is a product id from product card (product id)>> " +
                                      Provider.of<CartModel>(context)
                                          .productsInCart[item.id.toString()]
                                          .toString());
                            },
                            color: Colors.blueAccent,
                            child: Text(
                              S.of(context).addToBag,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14),
                            )),

                      SizedBox(width: 10),
                      if (showCart && !item.isEmptyProduct())
                        Center(
                            child: Container(
                          height: 33,
                          width: 39,
                          child: Stack(
                            children: <Widget>[
                              Badge(
                                child: Icon(Icons.add_shopping_cart, size: 39),
                                value:
                                Provider.of<CartModel>(context)
                                            .productsInCart[item.id.toString()]
                                            .toString() ==
                                        null
                                    ? ""
                                    : Provider.of<CartModel>(context)
                                        .productsInCart[item.id.toString()]
                                        .toString(),
                                color: Colors.redAccent,
                              )

//                              IconButton(
//                                  padding: EdgeInsets.only(right: 0.0),
//                                  icon: Icon(Icons.add_shopping_cart, size: 39),
//                                  onPressed: () {
//                                    // addProductToCart(product: item);
//                                  }),
//                              Visibility(
//                                visible: true,
//                                child:
//                                ,
//                                Align(
//                                  alignment: Alignment.topCenter,
//                                  child: Container(
//                                    height: 19,
//                                    width: 19,
//                                    decoration: BoxDecoration(
//                                        shape: BoxShape.circle,
//                                        color: Colors.redAccent),
//                                    child: Container(
//                                      child: Center(
//                                        child: Text(
//                                          counter.toString(),
//                                          textAlign: TextAlign.start,
//                                          style: TextStyle(
//                                            fontSize: 10,
//                                            color: Colors.white,
//                                          ),
//                                        ),
//                                      ),
//                                    ),
//                                  ),
//                                ),
                            ],
                          ),
                        ))
                    ],
                  ),
                ),
              ],
            ),
          ),
//        if (showHeart && !item.isEmptyProduct())
//          Positioned(
//            top: 10,
//            right: 10,
//            child: HeartButton(product: item, size: 18),
//          )
        ],
      ),
    );
  }
}
