import 'package:flutter/material.dart';
import 'package:fstore/models/cart.dart';
import 'package:fstore/screens/checkout/shipping_address.dart';
import 'package:shared_preferences/shared_preferences.dart';
class Badge extends StatefulWidget {
  const Badge({
    Key key,
    @required this.child,
    @required this.value,
    this.color,
  }) : super(key: key);

  final Widget child;
  final String value;
  final Color color;

  @override
  _BadgeState createState() => _BadgeState();
}

class _BadgeState extends State<Badge> {
  SharedPreferences preferences ;
  Future<void>  StoreData () async {
    return await SharedPreferences.getInstance() ;

  }


//  @override
//  void didChangeDependencies() async {
//   await  StoreData() ;
//   setState(() {
//     preferences.setString("ORDERS", widget.value == null ? "0" : widget.value) ;
//   });
//
//    super.didChangeDependencies();
//  }
  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }

    // TODO according to DartDoc num.parse() includes both (double.parse and int.parse)
    return double.parse(s, (e) => null) != null ||
        int.parse(s, onError: (e) => null) != null;
  }

  @override
  Widget build(BuildContext context) {

    return Stack(
      alignment: Alignment.center,
      children: [
        widget.child,
        Visibility(
          visible: isNumeric(widget.value) ,
          child: Align(
            alignment: Alignment.topCenter,
            child: Container(
              height: 19,
              width: 19,
              padding: EdgeInsets.all(2.0),
              // color: Theme.of(context).accentColor,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: widget.color != null ? widget.color : Theme.of(context).accentColor,
              ),
              constraints: BoxConstraints(
                minWidth: 16,
                minHeight: 16,
              ),
              child: Text(
               widget.value == null ? 0 : widget.value ,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 10,
                  color: Colors.white
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
