import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:fstore/generated/i18n.dart';
import 'package:fstore/providers/RatingProvider.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:rate_my_app/rate_my_app.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WepWithLocationView extends StatefulWidget {
  String currentLocation;
  Function function;
  BuildContext buildContext ;

  WepWithLocationView({this.currentLocation, this.function , this.buildContext});

  @override
  _WepWithLocationViewState createState() => _WepWithLocationViewState();
}

class _WepWithLocationViewState extends State<WepWithLocationView> {

  SharedPreferences prefs;

  @override
  void didChangeDependencies() async {
    prefs = await SharedPreferences.getInstance();

    super.didChangeDependencies() ;
  }

  RateMyApp _rateMyApp = RateMyApp(
    minDays: 7,
    minLaunches: 10,
    remindDays: 7,
    remindLaunches: 10,
    appStoreIdentifier: "",
    googlePlayIdentifier: "com.ea.bestclean",
  );

  @override
  void initState() {
    _rateMyApp.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          height: 10,
        ),
        Text(
          S.current.oneMoreStep,
          style: TextStyle(
              color: Colors.green, fontWeight: FontWeight.bold, fontSize: 28),
        ),
        SizedBox(
          height: 20,
        ),
        FittedBox(
          fit: BoxFit.contain,
          child: Text(
            S.current.sendLocation,
            style: TextStyle(
              color: Colors.green,
              fontWeight: FontWeight.bold,
              fontSize: 14

            ),
            softWrap: true,
            textAlign:TextAlign.center,

          ),
        ),
        SizedBox(
          height: 5,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                child: AutoSizeText(

              S.current.orderLocation,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color: Colors.black),
            )),
            SizedBox(
              width: 10,
            ),
            Container(
              height: 40,
              width: 40,
              child: FloatingActionButton(
                child: Icon(MdiIcons.mapMarker),
                onPressed: () {
                  _rateMyApp.showStarRateDialog(context,
                      title: 'What did you think of',
                      message: 'BestClean ?', onRatingChanged: (stars) {
                    return [
                      FlatButton(
                        child: Text('Ok'),
                        onPressed: () {
                          if (stars != null) {
                            _rateMyApp.doNotOpenAgain = true;
                            _rateMyApp.save().then((v) {
                              Navigator.of(context).pop();
                              widget.function();
                            });

                            if (stars <= 3) {
                              Provider.of<RatingProvider>(context)
                                  .setRating(true);
                              print('Navigate to Contact Us Screen');
                            } else if (stars <= 5) {
                              //   Provider.of<RatingProvider>(context).setRating(true) ;
                              print('Leave a Review Dialog');
                              Navigator.pop(context);
                              // showDialog(...);
                            }
                          } else {
                            // widget.function();
                            //    Provider.of<RatingProvider>(context).setRating(true) ;

                            //   widget.function();

                          }
                          // widget.function();
                        },
                      ),
                    ];
                  });

//                await LocationService().getLocation().then((value) async {
//                  widget.currentLocation =
//                      "https://maps.google.com/?q=${value.latitude},${value.longitude}";
////                            FlutterShareMe()
////                                .shareToWhatsApp(msg: currentAddress);
//
//                  await canLaunch(whatsappUrl)
//                      ? launch(whatsappUrl).then((_) {
//                          Future.delayed(Duration(seconds: 10)).then((value) {
//                           );
//                        })
//                      : await SendLocation().sendSMS(
//                          message: widget.currentLocation,
//                          recipents: ["+971554414585"]).then((_) {
//                          Future.delayed(Duration(seconds: 10)).then((value) {
//                            _rateMyApp.showStarRateDialog(context,
//                                title: 'Enjoying Flutter Rating Prompt?',
//                                message: 'Please leave a rating!',
//                                onRatingChanged: (stars) {
//                              return [
//                                FlatButton(
//                                  child: Text('Ok'),
//                                  onPressed: () {
//                                    if (stars != null) {
//                                      _rateMyApp.doNotOpenAgain = true;
//                                      _rateMyApp.save().then((v) {
//                                        Navigator.pop(context);
//                                        widget.function();
//                                      });
//                                      if (stars <= 3) {
//                                        print('Navigate to Contact Us Screen');
//                                        // Navigator.push(
//                                        //   context,
//                                        //   MaterialPageRoute(
//                                        //     builder: (_) => ContactUsScreen(),
//                                        //   ),
//                                        // );
//                                      } else if (stars <= 5) {
//                                        print('Leave a Review Dialog');
//                                        // showDialog(...);
//                                      }
//                                    } else {
//                                      Navigator.pop(context);
//                                      widget.function();
//                                    }
//                                    widget.function() ;
//                                  },
//                                ),
//                              ];
//                            });
//                          });
//                        });
//                });
                },
              ),
            )
          ],
        ),
      ],
    ));
  }
}
