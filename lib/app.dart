import 'dart:async';
import 'dart:ui';

import 'package:after_layout/after_layout.dart';
import 'package:custom_splash/custom_splash.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flare_splash_screen/flare_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:fstore/providers/RatingProvider.dart';
import 'package:fstore/screens/login_sms/index.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';
import './providers/ArabicProvider.dart';

import 'common/config.dart';
import 'common/constants.dart';
import 'common/styles.dart';
import 'common/tools.dart';
import 'generated/i18n.dart';
import 'models/app.dart';
import 'models/blog.dart';
import 'models/cart.dart';
import 'models/category.dart';
import 'models/order.dart';
import 'models/payment_method.dart';
import 'models/product.dart';
import 'models/recent_product.dart';
import 'models/search.dart';
import 'models/shipping_method.dart';
import 'models/user.dart';
import 'models/wishlist.dart';
import 'screens/blogs.dart';
import 'screens/checkout/index.dart';
import 'screens/login.dart';
import 'screens/notification.dart';
import 'screens/onboard_screen.dart';
import 'screens/orders.dart';
import 'screens/products.dart';
import 'screens/registration.dart';
import 'screens/wishlist.dart';
import 'services/index.dart';
import 'services/wordpress.dart';
import 'tabbar.dart';

FirebaseAnalytics analytics = FirebaseAnalytics();

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.dark
          .copyWith(statusBarColor: Theme.of(context).accentColor),
    );

    /// For Flare Image
    if (kSplashScreen.lastIndexOf('flr') > 0) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SplashScreen.navigate(
          name: kSplashScreen,
          startAnimation: 'Best Clean',
          backgroundColor: Colors.white,
          next: (object) => MyApp(),
          until: () => Future.delayed(Duration(seconds: 2)),
        ),
      );
    }

    return MultiProvider(providers: [
      ChangeNotifierProvider.value(value: SwitchCategoryProvider()),
      ChangeNotifierProvider.value(value: RatingProvider()) ,
    ],
    child: MaterialApp(
      debugShowCheckedModeBanner: false,

      home: CustomSplash(
        imagePath: kLogoImage,
        backGroundColor: Colors.white,
        animationEffect: 'fade-in',
        logoSize: 120,
        home: MyApp(),
        duration: 3500,
      ),
    ),) ;
  }
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

class AppState extends State<MyApp> with AfterLayoutMixin {
  final _app = AppModel();
  final _product = ProductModel();
  final _wishlist = WishListModel();
  final _shippingMethod = ShippingMethodModel();
  final _paymentMethod = PaymentMethodModel();
  final _order = OrderModel();
  final _search = SearchModel();
  final _recent = RecentModel();
  final _blog = BlogModel();
  bool isFirstSeen = false;
  bool isChecking = true;
  bool isLoggedIn = false;

  @override
  void afterFirstLayout(BuildContext context) async {
    Services().setAppConfig(serverConfig);
    WordPress().setAppConfig(serverConfig);
    _app.loadAppConfig();

    isFirstSeen = await checkFirstSeen();
    isLoggedIn = await checkLogin();
    setState(() {
      isChecking = false;
    });
  }

  Future checkFirstSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _seen = (prefs.getBool('seen') ?? false);

    if (_seen)
      return false;
    else {
      prefs.setBool('seen', true);
      return true;
    }
  }

  Future checkLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('loggedIn') ?? false;
  }

  Widget renderFirstScreen() {
    if (isFirstSeen) return OnBoardScreen();
    if (kAdvanceConfig['IsRequiredLogin'] && !isLoggedIn) return LoginSMS();
    return MainTabs();
  }

  @override
  Widget build(BuildContext context) {
    if (isChecking) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          body: Container(),
        ),
      );
    }

    return ChangeNotifierProvider<AppModel>.value(
      value: _app,
      child: Consumer<AppModel>(
        builder: (context, value, child) {
          if (value.isLoading) {
            return Container(
              color: Colors.white,
            );
          }
          return MultiProvider(
            providers: [
              Provider<ProductModel>.value(value: _product),
              Provider<WishListModel>.value(value: _wishlist),
              Provider<ShippingMethodModel>.value(value: _shippingMethod),
              Provider<PaymentMethodModel>.value(value: _paymentMethod),
              Provider<OrderModel>.value(value: _order),
              Provider<SearchModel>.value(value: _search),
              Provider<RecentModel>.value(value: _recent),
              ChangeNotifierProvider(create: (_) => UserModel()),
              ChangeNotifierProvider(create: (_) => CartModel()),
              ChangeNotifierProvider(create: (_) => CategoryModel()),
              ChangeNotifierProvider(create: (_) => _blog)
            ],
            child: MaterialApp(
              debugShowCheckedModeBanner: false,
              locale: new Locale(Provider.of<AppModel>(context).locale, ""),
              navigatorObservers: [
                FirebaseAnalyticsObserver(analytics: analytics),
              ],
              localizationsDelegates: [
                S.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              supportedLocales: S.delegate.supportedLocales,
              localeListResolutionCallback:
                  S.delegate.listResolution(fallback: const Locale('en', '')),
              home: renderFirstScreen(),
              routes: <String, WidgetBuilder>{
                "/home": (context) => MainTabs(),
                "/login": (context) => LoginScreen(),
                "/register": (context) => RegistrationScreen(),
                '/products': (context) => ProductsPage(),
                '/wishlist': (context) => WishList(),
                '/checkout': (context) => Checkout(),
                '/orders': (context) => MyOrders(),
                '/blogs': (context) => BlogScreen(),
                '/notify': (context) => Notifications() ,
                '/phoneAuth' :(ctx) => LoginSMS() ,
              },
              theme: Provider.of<AppModel>(context).darkTheme
                  ? buildDarkTheme().copyWith(
                      primaryColor:
                        HexColor(_app.appConfig["Setting"]["MainColor"])


              )
                  : buildLightTheme().copyWith(
                    //  primaryColor: Colors.white
                     primaryColor: HexColor(_app.appConfig["Setting"]["MainColor"])
              ),
            ),
          );
        },
      ),
    );
  }
}
