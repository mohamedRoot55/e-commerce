import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SwitchCategoryProvider with ChangeNotifier {
  bool _isArabic = false;
  SharedPreferences prefs ;

  void SwtistToArabic() async {
    _isArabic = true;
    prefs =  await SharedPreferences.getInstance() ;
    prefs.setBool("is_arabic", _isArabic) ;
    notifyListeners();
  }
  void SwtistToEnglish() {
    _isArabic = false;
    prefs.setBool("is_arabic", _isArabic) ;
    notifyListeners();
  }

  bool get getSwisherValue {
    return _isArabic;
  }
}
