import 'package:flutter/cupertino.dart';

class RatingProvider with ChangeNotifier {
  bool isRating = false ;

  void setRating (bool value){
    isRating = value ;
    notifyListeners() ;
  }
  bool getRating (){
    return isRating ;
  }
}