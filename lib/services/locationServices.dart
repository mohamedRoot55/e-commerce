import 'dart:async';


import 'package:fstore/models/UserLocation.dart';
import 'package:location/location.dart';

class LocationService {
  UserLocation userLocation;

  var location = Location();
  String error = '';

  LocationData _currentLocation;

  Future<UserLocation> getLocation() async {
    //  var location = new Location();

// Platform messages may fail, so we use a try/catch PlatformException.
    try {
      _currentLocation = await location.getLocation().whenComplete(() {
        location.changeSettings(
            accuracy: LocationAccuracy.HIGH, distanceFilter: 0, interval: 1000);
      });

      userLocation = new UserLocation(
          latitude: _currentLocation.latitude,
          longitude: _currentLocation.longitude);
    } on Exception catch (e) {
      if (e.toString() == 'PERMISSION_DENIED') {
        error = 'Permission denied';
      }
      userLocation = null;
    }
    return userLocation;
  }
}
