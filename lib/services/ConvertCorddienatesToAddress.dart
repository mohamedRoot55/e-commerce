
import 'package:geocoder/geocoder.dart';

class ConvertToAddress {
  //todo convert coordinates to readable adress

  Future<String> ConvertCoordinatesToAddress(
      double latitude, double longitude) async {
    String address = '';

    final coordinates = new Coordinates(latitude, longitude);
    var addresses =
    await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    address = first.addressLine;
    return address;
  }

}